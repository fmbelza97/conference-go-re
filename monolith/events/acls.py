import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_city_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    payload = {
        "query": f"{city} {state}",
        "per_page": 1,
    }
    response = requests.get(url, params=payload, headers=headers)
    photo_dict = json.loads(response.content)
    return photo_dict["photos"][0]["url"]


def get_weather_data(city, state):
    # Geocode
    url = "http://api.openweathermap.org/geo/1.0/direct"

    payload = {
        "q": f"{city},{state},{840}",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    response = requests.get(url, params=payload)
    content = json.loads(response.content)
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    # Currentweatherapi
    url = "https://api.openweathermap.org/data/2.5/weather"

    payload = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    # make the request
    response = requests.get(url, params=payload)
    content = json.loads(response.content)
    try:
        weatherinfo = {
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }
        return weatherinfo
    except (KeyError, IndexError):
        return None
